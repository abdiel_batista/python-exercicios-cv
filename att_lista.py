num = [1, 4, 5, 8]
num[2] = 6
num.append(9)
num.insert(4, 3)
num.remove(6)
if 2 in num:
    num.remove(2)
print(num)

n = []
n.append(1)
n.append(4)
n.append(8)
n.append(3)
n.append(9)

for c in n:
    print(c, end=' ')

for c, v in enumerate(n):
    print('Na posição {} encontrei {}'.format(c, v,))

nums = []
for c in range(5):
    nums.append(int(input('Digite cinco valores ')))
print(nums)
