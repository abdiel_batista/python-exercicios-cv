"""crie um algoritmo que leia um número e mostre seu dobro, triplo, e raiz"""

from math import sqrt

num = int(input('Digite um valor: '))
dobro = num * 2
triplo = num * 3
raiz = sqrt(num)

print('O dobro desse número é: {}'.format(dobro))
print('O triplo: {}'.format(triplo))
print('E a raiz: {}'.format(raiz))
