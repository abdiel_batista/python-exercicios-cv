"""converter metros para centímetros e milímetros"""

metro = float(input('Digite um valor em metros: '))
cm = metro * 100
mm = metro * 1000
print('A medida {}m corresponde a {}cm e {}mm'.format(metro, cm, mm))
