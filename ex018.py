""" Seno, Cosseno e Tangente """

from math import sin, cos, tan, radians
angulo = float(input('digite o ângulo: '))

r = radians(angulo)
s = sin(r)
c = cos(r)
t = tan(r)

print('o ângulo de', angulo, 'tem o SENO de', s)
print('o ângulo de', angulo, 'tem COSSENO de', c)
print('o ângulo de', angulo, 'tem TANGENTE de', t)