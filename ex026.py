""" Analisando frase """

f = str(input('Digite uma frase: ')).strip().upper()
print('A letra A aparece {} vezes'.format(f.count('A')))
print('A letra A aparece a primeira vez na posição {}'.format(f.find('A')+1))
print('A letra A aparece a última vez na posição {}'.format(f.rfind('A')))
