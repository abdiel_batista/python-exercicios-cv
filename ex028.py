""" JOGO """
from random import randint
from time import sleep
sort = randint(0 ,5)
print('\033[31;m-==-' * 10)
print('\033[33;mVou pensar em um número entre 0 e 5')
print('\033[31;m-==-' * 10)
r = int(input('\033[33;mEm qual número estou pensando: '))
print('\033[35;mANALISANDO...')
sleep(2)
if r == sort:
    print('Você venceu, eu estava pensando no {}'.format(sort))
else:
    print('Você erro, eu estava pensando no {}.'.format(sort))
