print('=== RADAR ELETRÔNICO ===')

km = int(input('Qual a velocidade do carro ? '))

if km > 80:
    print('Acima da velocidade, você foi mutado em R${:.2f}'.format((km - 80) * 7))
else:
    print('Carro na velocidade permitida!')
