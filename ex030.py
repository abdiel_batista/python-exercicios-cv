""" Par ou ímpar"""

n = int(input('Digite um número: '))

if n % 2 == 0:
    print('{} é PAR'.format(n))
else:
    print('{} é ÍMPAR'.format(n))
