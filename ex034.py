""" AUMENTO DO SÁLARIO """

sa = float(input('Qual o valor do sálario ? '))

if sa > 1250.00:
    print('O salário de R${} irá p/ R${} com reajuste de 10%.'.format(sa, (sa * 10) / 100 + sa))
else:
    print('O salário de R${} irá p/ R${} com reajuste de 10%.'.format(sa, (sa * 15) / 100 + sa))
