nome = str(input('Digite o nome do aluno: ')).capitalize()
n1 = float(input('Primeira nota: '))
n2 = float(input('Segunda nota: '))
n3 = float(input('Terceira nota: '))

nf = n1 + n2 + n3
if nf >= 60:
    print('A nota final do aluno, {}, é: {} e ele esta APROVADO'.format(nome, nf))
elif nf < 50:
    print('A nota final do aluno, {}, é: {} e ele esta REPROVADO'.format(nome, nf))
else:
    print('A nota final do aluno, {}, é: {} e ele esta de RECUPERAÇÃO'.format(nome, nf))
