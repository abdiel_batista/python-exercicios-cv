num = int(input('Digite um número inteiro: '))
print('''== Escolha uma das bases de conversão ==
[ 1 ] converte para BINÁRIO 
[ 2 ] converte para OCTAL
[ 3 ] converte para HEXADECIMAL''')
opcao = int(input('Sua escolha: '))

if opcao == 1:
    print('{} em BINÁRIO é igual a: {}'.format(num, bin(num)[2:]))
elif opcao == 2:
    print('{} em OCTAL é igual a: {}'.format(num, oct(num)[2:]))
elif opcao == 3:
    print('{} em HEXADECIMAL é igual a: {}'.format(num, hex(num)[2:]))
else:
    print('ERRO')
