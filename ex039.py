from datetime import date
ano = date.today().year
a = int(input('Ano de nascimento: '))
idade = ano - a
print('Quem nasceu em {} tem {} anos em {}.'.format(a, idade, ano))
if idade == 18:
    print('Você tem que se alistar IMEDIATAMENTE!')
elif idade < 18:
    saldo1 = 18 - idade
    print('Ainda faltam {} anos para o alistamento.'.format(saldo1))
    print('Seu alistamento será em {}.'.format(ano+saldo1))
else:
    saldo2 = idade - 18
    print('Você deveria ter se alistado a {} anos.'.format(saldo2))
    print('Seu alistamento foi em {}.'.format(ano-saldo2))
