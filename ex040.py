""" nota do aluno """
nota1 = float(input('Primeira nota: '))
nota2 = float(input('Segunda nota: '))
media = (nota1 + nota2) / 2
if media < 5.0:
    print('aluno \033[31;1;mREPROVADO.\033[m')
elif media > 4.9 and media < 7.0:
    print('aluno em \033[34;1;mRECUPERAÇÃO.\033[m')
else:
    print('aluno \033[30;1;mAPROVADO.\033[m')
