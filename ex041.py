from datetime import date
a = date.today().year
ano = int(input('Ano de nascimento do ATLETA: '))
idade = a - ano
print('Quem nasceu em {}, tem {} anos.'.format(ano, idade))
if idade < 10:
    print('ATLETA na categoria: MIRIN.')
elif idade > 9 and idade <= 14:
    print('ATLETA na categoria: INFANTIL.')
elif idade > 14 and idade <= 19:
    print('ATLETA na categoria: JUNIOR.')
elif idade > 19 and idade <= 20:
    print('ATLETA na categoria: SÊNIOR.')
else:
    print('ATLETA na categoria: MASTER')
