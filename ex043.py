""" IMC """
peso = float(input('PESO: '))
alt = float(input('ALTURA: '))
imc = peso / alt ** 2
print('O IMC é {:.1f}'.format(imc))
if imc < 18.5:
    print('ABAIXO DO PESO.')
elif imc > 18.4 and imc < 24.9:
    print('PESO IDEAL.')
elif imc > 24 and imc < 31:
    print('SOBREPESO.')
elif imc > 29 and imc < 41:
    print('ACIMA DO PESO.')
else:
    print('OBESIDADE MORBIDA')
