""" desconto """
p = float(input('Qual o preço do produto ? R$'))
print('''FORMAS DE PAGAMENTO
[ 1 ] à vista dinheiro/cheque
[ 2 ] à vista no cartão
[ 3 ] 2x no cartão 
[ 4 ] 3x ou mais no cartão''')
resp = int(input('QUAL A OPÇÃO ? '))
if resp == 1:
    des = p * 10 / 100
    print('Sua compra de R${:.2f}, vai custar R${:.2f}.'.format(p, p - des))
elif resp == 2:
    des2 = p * 5 / 100
    print('Sua compra de R${:.2f}, vai custar R${:.2f}.'.format(p, p - des2))
else:
    au = p * 20 / 100
    print('Sua compra de R${:.2f}, COM JUROS irá para R${:.2f}'.format(p, au + p))
