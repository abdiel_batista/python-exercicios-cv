from random import randint
s = ['PEDRA', 'PAPEL', 'TESOURA']
pc = randint(0, 2)
print('\033[31;1;m=' * 37)
print('{:^36}'.format('JOKENPÔ'))
print('=' * 37)
print('\033[1;33;m[ 0 ]PEDRA \n\033[1;34;m[ 1 ]PAPEL \n\033[1;30;m[ 2 ]TESOURA\033[m ')
resp = int(input('\033[31;1;mQual sua JOGADA ? \033[m'))
print('\033[1;36;mComputador: {}'.format(s[pc]))
print('Você: {}\033[m'.format(s[resp]))
if pc == 0: #compuutador jogou pedra
    if resp == 0:
        print('\033[1;33;mEmpate, tente novamente.')
    elif resp == 1:
        print('\033[1;33;mVOCÊ VENCEU!')
    elif resp == 2:
        print('\033[1;33;mVOCÊ PERDEU!')
    else:
        print('ERRO')
elif pc == 1: #computador jogou papel
    if resp == 1:
        print('\033[1;33;mEmpate, tente novamente.')
    elif resp == 0:
        print('\033[1;33;mVOCÊ VENCEU!')
    elif resp == 2:
        print('\033[1;33;mVOCÊ VENCEU!')
    else:
        print('ERRO')
elif pc == 2: #computador jogou tesoura
    if resp == 2:
        print('\033[1;33;mEmpate, tente novamente.')
    elif resp == 0:
        print('\033[1;33;mVOCÊ VENCEU!')
    elif resp == 1:
        print('\033[1;33;mVOCÊ PERDEU!')
