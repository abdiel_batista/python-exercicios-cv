""" Número pares de 0 a 50"""
'''for c in range(0, 52, 2): # primeira forma de fazer
    print(c)'''
c = 0
for c in range(1, 51): # segunda forma de fazer
    if c % 2 == 0:
        print(c, end=' ')
print('FIM')
