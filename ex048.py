""" Somar os números ímpares de 0 a 500"""
s = 0
cont = 0
for c in range(0, 501):
    if c % 2 == 1 and c % 3 == 0:
        cont = cont + 1
        s = s + c
print('A soma dos {} valores é {}.'.format(cont, s))
