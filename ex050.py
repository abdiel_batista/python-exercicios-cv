""" Ler seis número e somar os pares """
s = 0
for c in range(1, 7):
    num = int(input('Digite o {}º número: '.format(c)))
    if num % 2 == 0:
      s = num + s
print('A soma dos números pares é: {}'.format(s))
