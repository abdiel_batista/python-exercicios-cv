num = int(input('Digite um número: '))
vezes = 0
for c in range(1, num+1):
    if num % c == 0:
        print('\033[33;m', end='')
        vezes += 1
    else:
        print('\033[31;m', end='')
    print(c, end=' ')
print('\n\033[mO número {} foi divisível {} vezes.'.format(num, vezes))
if vezes == 2:
    print('E por isso ele É PRIMO!')
else:
    print('E por isso ele NÃO É PRIMO')
