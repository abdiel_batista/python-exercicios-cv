""" maior de idade """
from datetime import date
ano = date.today().year
maior = 0
menor = 0
for c in range(1, 8):
    nas = int(input('Ano de nascimento da {}ª pessoa: '.format(c)))
    if ano - nas > 20:
        maior += 1
    else:
        menor += 1
print('{} pessoas são de maiores, e {} ainda são de menores.'.format(maior, menor))
