""" Peso maior e menor """
pesos = []
for c in range(1, 6):
    p = float(input('Peso da {}ª pessoa: '.format(c)))
    pesos.append(p)
print('O maior peso lido foi {}'.format(max(pesos)))
print('E o menor foi {}'.format(min(pesos)))
