soma_idade = 0
media_idade = 0
maior_i = 0
nome_maior = 0
totm = 0
for c in range(1, 5):
    print('----- {}ª PESSOA -----'.format(c))
    n = str(input('Nome: ')).strip()
    i = int(input('Idade: '))
    s = str(input('Sexo [M/F: ')).strip()
    soma_idade = soma_idade + i
    if c == 1 and s in 'Mm':
        maior_i = i
        nome_maior = n
    if s in 'Mm' and i > maior_i:
        maior_i = i
        nome_maior = n
    if s in 'Ff' and i < 20:
        totm = totm + 1
media_idade = soma_idade / 4
print('A média de idade do grupo é {}'.format(media_idade))
print('O homem mais velho tem {} anos, e se chama.'.format(maior_i, nome_maior))
print('Ao todo são {} mulheres com mais de 20 anos'.format(totm))
