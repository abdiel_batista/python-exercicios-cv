from random import randint
pc = randint(0, 10)
print('''Sou seu computador...
Acabei de pensa em um número de 0 a 10.
Tente adivinhar...''')
acertou = False
palpite = 0
while not acertou:
    resp = int(input('QUAL O SEU PALPITE ? '))
    palpite = palpite + 1
    if resp == pc:
        acertou = True
    if resp < pc:
        print('Mais... Tente novamente.')
    if resp > pc:
        print('Menos... Tente novamente')
print('Acertou com {} tentativas. Parabéns!!!'.format(palpite))
