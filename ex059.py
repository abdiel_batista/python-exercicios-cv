""" CALCULADORA """
from time import sleep
resp = 6
while resp != 5:
    num1 = int(input('Primeiro valor: '))
    num2 = int(input('Segundo valor: '))
    print('''VOCÊ DESEJA: 
    [ 1 ] SOMAR
    [ 2 ] MULTIPLICAR
    [ 3 ] MAIOR
    [ 4 ] NOVOS VALORES
    [ 5 ] SAIR DO PROGRAMA''')
    resp = int(input('>>> OPÇÃO: '))
    if resp == 1:
        print('A soma de {} + {} é: {}'.format(num1, num2, num1 + num2))
    elif resp == 2:
        print('A multiplicação de {} e {} é {}'.format(num1, num2, num1*num2))
    elif resp == 3:
        if num1 > num2:
            print('Entre {} e {} o maior valor é {}.'.format(num1, num2, num1))
        elif num2 > num1:
            print('Entre {} e {} o maior valor é {}'.format(num1, num2, num2))
        else:
            print('Os valores são iguais.')
    elif resp == 5:
        print('Encerrando...')
    if resp > 5:
        print('Opção inválida')
    print('==-' * 15)
sleep(2)
