""" FATORIAL """
from math import factorial
n = int(input('Digite um número para calcular seu Fatorial: '))
c = n
f = factorial(n)
print('{}! = '.format(n), end='')
while c > 0:
    print('{}'.format(c), end='')
    print(' x ' if c > 1 else ' = {}'.format(f), end='')
    c = c - 1

# print('O fatoria de {} é {}.'.format(n, f))
