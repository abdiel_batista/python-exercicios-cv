pt = int(input('Digite o primeiro termo da P.A: '))
rz = int(input('Digite a razão da P.A: '))
termo = pt
cont = 1
total = 0
mais = 10
while mais != 0:
    total = total + mais
    while cont <= total:
        print('{} -> '.format(termo), end='')
        termo = termo + rz
        cont = cont + 1
    print('PAUSA')
    mais = int(input('Quantos termos você quer mostrar a mais ? '))
print('Progressão finalizada com {} termos'.format(total))