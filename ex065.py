""" MAIOR E MÉDIA """
r = 'S'
m = 0
n = 0
s = 0
n2 = 0
cont = 0
maior = 0
menor = 0
while r in 'Ss':
    num = int(input('Digite um número: '))
    s = num + s
    n = n + 1
    if n == 1:
        maior = menor = num
    else:
        if num > maior:
            maior = num
        if num < menor:
            menor = num

    r = str(input('Quer continuar ? [S/N] ')).upper().strip()[0]
m = s / n

print('Você digitou {} números e a média foi: {:.2f}'.format(n, m))
print('O maior número digitado foi {}'.format(maior))
print('E o menor número digitado foi {}'.format(menor))
