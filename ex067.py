""" TABUADA """
n = 0
c = 0
while True:
    n = int(input('Você que ver a tabuadada de qual número: '))
    print('-' * 30)
    if n < 0:
        break
    for c in range(1, 11):
        print('{} x {:2} = {}'.format(n, c, n * c))
    print('-' * 30)
