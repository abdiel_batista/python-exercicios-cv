from random import randint
pontos = 0
print('''=-=''' * 9)
print('VAMOS JOGAR PAR OU ÍMPAR!')
while True:
    print('''=-=''' * 9)
    valor_p = int(input('Digite um valor: '))
    resp = str(input('Par ou Ímpar ? [P/I] ')).upper().strip()[0]
    cpu = randint(0, 10)
    s = valor_p + cpu
    if s % 2 == 0:
        par_impa = 'PAR'
        P = 'P'
    elif s % 2 == 1:
        par_impa = 'ÍMPAR'
        P = 'I'
    print('Você jogou {} e o PC {}. Total de {}, DEU {}.'.format(valor_p, cpu, s, par_impa))
    print('''=-=''' * 15)
    if resp == P:
        print('VOCÊ VENCEU.')
        print('Vamos jogar novamente...')
        pontos = pontos + 1
    else:
        print('VOCÊ PERDEU.')
        print('GAME OVER, HUMANO. Você venceu {} vezes.'.format(pontos))
        break
