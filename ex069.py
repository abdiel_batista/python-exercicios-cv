p = 0
h = 0
m = 0
while True:
    print('---'*7)
    print('CADASTRE UMA PESSOA')
    print('---'*7)
    idade = int(input('Idade: '))
    sexo = 'AB'
    resp = 'AB'
    if idade > 18:
        p = p + 1
    while sexo not in 'MF':
        sexo = str(input('Sexo: [M/F] ')).upper().strip()[0]
    if sexo == 'M':
        h = h + 1
    if idade < 20 and sexo == 'F':
        m = m + 1
    while resp not in 'SN':
        print('---' * 7)
        resp = str(input('Quer continuar ? [S/N] ')).upper().strip()[0]
    if resp == 'N':
        break
print('Total de pessoas com mais de 18 anos: {}'.format(p))
print('Temos {} homens cadastrados.'.format(h))
print('E {} mulheres com menos de 20 anos cadastradas.'.format(m))
