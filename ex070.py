print('~~'*15)
print('        LOJAS ABDIEL')
print('~~'*15)
total = 0
mais = 0
menor = 0
cont = 0
barato = ' '
while True:
    nome = str(input('Nome do produto: ')).title()
    preco = float(input('Preço: R$'))
    cont = cont + 1
    total = preco + total
    resp = 'AB'
    if preco > 1000:
        mais = mais + 1
    if cont == 1:
        menor = preco
        barato = nome
    else:
        if preco < menor:
            menor = preco
            barato = nome
    while resp not in 'SN':
        resp = str(input('Quer continuar ? [S/N] ')).upper().strip()[0]
    if resp == 'N':
        print('---- FIM DO PROGRAMA ----')
        break

print('O total da compra foi R${:.2f}'.format(total))
print('Temos {} produtos custando mais de R$1000.00'.format(mais))
print('O produto mais barato foi {} custando R${:.2f}'.format(barato, menor))
