print('='*30)
print('{:^30}'.format('BANCO BATISTA'))
print('='*30)
num = int(input('Que valor você quer sacar ? R$'))
for cedula in (50, 20, 10, 1):
    print('Total de {} cédulas de R${}'.format(num//cedula, cedula))
    num = num % cedula
