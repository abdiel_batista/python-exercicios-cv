""" NÚMERO POR EXTENSO """
num = ('zero', 'um', 'dois', 'três', 'quatro', 'cinco',
       'seis', 'sete', 'oito', 'nove', 'dez',
       'onze', 'doze', 'treze', 'catorze', 'quinze',
       'dezesseis', 'dezessete', 'dezoito', 'dezenove', 'vinte')

resp = int(input('Digite um número entre 0 e 20: '))
while resp not in num[resp]:
        resp = int(input('Tente novamente. Digite um número entre 0 e 20: '))
print('Você digitou o número {}.'.format(num[resp]))