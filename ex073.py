""" LISTAS DE FILMES """
filmes = ('Avatar', 'Titanic', 'Star Wars VII', 'Avengers: Infinity War', 'Jurassic World', 'The Avengers',
          'Furious 7', 'Avengers: Age of Ultron', 'Black Panther', 'Harry Potter', 'Star Wars VIII', 'Frozen',
          'Beauty and the Beast', 'The Fate of the Furious', 'Iron Man 3', 'Jurassic World: Fallen Kingdom',
          'Minions', 'Captain America: Civil War', 'Transformers: Dark of the Moon', 'The Lord of the Rings 3')
print('Os 5 primeiros são: {}'.format(filmes[0:5]))
print('='*30)
print('Os últimos 4 são: {}'.format(filmes[-4:]))
print('='*30)
print('Filmes em ordem alfabética: {}'.format(sorted(filmes)))
print('='*30)
print('O filme Iron Man 3 está na {}ª posição'.format(filmes.index('Iron Man 3')+1))
print('='*30)
