""" MAIOR E MENOR """
from random import randint
n = randint(1, 10), randint(1, 10), randint(1, 10), randint(1, 10), randint(1, 10)
print('Os valores sorteados foram: ', end='')
for c in n:
    print('{}'.format(c), end=' ')
print('\nO maior valor sorteado foi {}'.format(max(n)))
print('O menor valor sorteado foi {}'.format(min(n)))
