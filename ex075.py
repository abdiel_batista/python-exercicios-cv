nove = 0
num1 = int(input('Digite um número: '))
num2 = int(input('Digite outro número: '))
num3 = int(input('Digite mais um número: '))
num4 = int(input('Digite o último número: '))
t = num1, num2, num3, num4
print('Voce digitou os valores {}'.format(t))
if 9 not in t:
    print('O valor 9 não foi inserido')
else:
    print('O valor 9 apareceu {} vezes'.format(t.count(9)))
if 3 not in t:
    print('O valor 3 não foi inserido')
else:
    print('O valor 3 apareceu na {}ª posição'.format(t.index(3)+1))
print('Os valores pares digitados são ', end='')
for n in t:
    if n % 2 == 0:
        print('{}'.format(n), end=' ')
