palavras = ('séries', 'filmes', 'got', 'sherlock',
            'java', 'python', 'programar', 'app',
            'violao', 'fone', 'notebook', 'anel')
for p in palavras:
    print('\nNa palavra {} temos '.format(p.upper()), end='')
    for l in p:
        if l.lower() in 'aeiou':
            print(l, end=' ')
