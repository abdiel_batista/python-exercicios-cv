"""" maior e menor em listas"""
valor = []
p = 0
for c in range(0, 5):
    valor.append(int(input('Digite um valor pra posição {}: '.format(c))))
print('='*30)
print('Você digitou os valores: {}'.format(valor))
print('O maior valor digitado foi {} na posição {}'.format(max(valor), valor.index(max(valor))))
print('O menor valor digitado foi {} na posição {}'.format(min(valor), valor.index(min(valor))))
