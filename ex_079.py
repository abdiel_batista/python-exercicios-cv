n = []
resp = 'S'
while resp == 'S':
    nums = (int(input('Digite um valor: ')))
    if nums not in n:
        n.append(nums)
        print('Valor adicionado com sucesso...')
    else:
        print('Valor já inserido...')
    resp = str(input('Quer continuar ? [S/N] ')).upper().strip()[0]
n.sort()
print('-='*20)
print('Você digitou os valores {}'.format(n))
