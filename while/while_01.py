condi = True # enquanto a condição for 'True' ira se repete
while condi:
    print('WHILE DA CONDIÇÃO = TRUE')
    condi = False  # recebeu 'False' e vai para o 'ELSE'
    break  # se while for falso break para Ele antes de chegar no 'ELSE'
else:
    print('WHILE DA CONDIÇÃO = FALSE')
